package com.cmu.jaylerr.bikesquad.main.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.cmu.jaylerr.bikesquad.R;
import com.cmu.jaylerr.bikesquad.main.presenter.MainThemePresenter;
import com.cmu.jaylerr.bikesquad.main.presenter.ProfileBarPresenter;
import com.cmu.jaylerr.bikesquad.models.Members;
import com.cmu.jaylerr.bikesquad.utility.dialogmodels.DialogAcceptanceUtility;
import com.cmu.jaylerr.bikesquad.utility.sharedstring.SharedFlag;
import com.cmu.jaylerr.bikesquad.utility.sharedstring.SharedKey;

import org.parceler.Parcels;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private View headerLayout;
    private Members members;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        headerLayout = navigationView.getHeaderView(0);

        getUser();
    }

    private void getUser(){
        Intent intent = getIntent();
        members = Parcels.unwrap(intent.getParcelableExtra(SharedKey._key_putExtra_member_to_main));
        setTheme();
        setProfileBar();
    }
    private void setProfileBar(){
        ProfileBarPresenter profileBarPresenter = new ProfileBarPresenter(this, headerLayout, members);
        profileBarPresenter.setProfileBarContent();
    }

    private void setTheme(){
        MainThemePresenter main = new MainThemePresenter(MainActivity.this, headerLayout);
        main.setProfileBarTheme();
        main.setToolBar();
    }

    private void profile(){

    }

    private void activity(){

    }

    private void route(){

    }

    private void friends(){

    }

    private void setting(){

    }

    private void signOut(){
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                DialogAcceptanceUtility dialog = new DialogAcceptanceUtility(MainActivity.this,
                        getString(R.string.dialog_title_confirm),
                        getString(R.string.account_message_sign_out),
                        SharedFlag._flag_sign_out);
                dialog.show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_profile) {
            profile();
        } else if (id == R.id.nav_activity) {
            activity();
        } else if (id == R.id.nav_route){
            route();
        }else if (id == R.id.nav_friend) {
            friends();
        } else if (id == R.id.nav_setting) {
            setting();
        } else if (id == R.id.nav_sign_out) {
            signOut();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
