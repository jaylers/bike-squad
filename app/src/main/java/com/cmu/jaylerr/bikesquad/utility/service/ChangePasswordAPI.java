package com.cmu.jaylerr.bikesquad.utility.service;

import com.cmu.jaylerr.bikesquad.models.modelsrequest.RegisterRequest;
import com.cmu.jaylerr.bikesquad.models.modelsrespond.RegisterRespond;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by jaylerr on 19-Jan-17.
 */

public interface ChangePasswordAPI {

    @POST("api/companyapp/public/activate/application")
    Call <RegisterRespond> getRegister(@Body RegisterRequest activateRequest);
}
