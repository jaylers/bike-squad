package com.cmu.jaylerr.bikesquad.authentication.views;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;

import com.cmu.jaylerr.bikesquad.R;
import com.cmu.jaylerr.bikesquad.authentication.background.UpdateUserTask;
import com.cmu.jaylerr.bikesquad.utility.dialogmodels.DialogAcceptanceUtility;
import com.cmu.jaylerr.bikesquad.utility.sharedpreference.StatePreference;
import com.cmu.jaylerr.bikesquad.utility.sharedstring.SharedFlag;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AuthenticationActivity extends AppCompatActivity {
    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler mHideHandler = new Handler();
    private View mContentView;
    private View mControlsView;
    @BindView(R.id.frame_fragment_content_control) FrameLayout fragment_content_control;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentication);
        ButterKnife.bind(this);

        mControlsView = findViewById(R.id.fullscreen_content_controls);
        mContentView = findViewById(R.id.fullscreen_content);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                userState();
            }
        }, 200);
    }

    private void userState(){
        StatePreference statePreference = new StatePreference(AuthenticationActivity.this);
        if (statePreference.getState()){
            //update user information
            updateUser();
        }else {
            //default fragment
            openSignIn();
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getCurrentFragment();
        if (fragment instanceof RegisterFragment){
            backToSignIn();
        }else if (fragment instanceof SignInFragment){
            DialogAcceptanceUtility dialogAcceptanceUtility = new DialogAcceptanceUtility(
                    AuthenticationActivity.this, getString(R.string.dialog_title_confirm),
                    getString(R.string.app_message_close_app), SharedFlag._flag_on_back_press);
            dialogAcceptanceUtility.show();
        }else {
            backToSignIn();
        }
    }
    private Fragment getCurrentFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        int stackCount = fragmentManager.getBackStackEntryCount();
        if( fragmentManager.getFragments() != null ) return fragmentManager.getFragments().get( stackCount > 0 ? stackCount-1 : stackCount );
        else return null;
    }

    private void backToSignIn(){
        SignInFragment signInFragment = new SignInFragment();
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_from_left,
                R.anim.slide_out_to_right);
        ft.replace(R.id.frame_fragment_content_control, signInFragment);
        ft.commit();
    }

    private void openSignIn(){
        SignInFragment signInFragment = new SignInFragment();
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        ft.setCustomAnimations(R.anim.fade_in,
                R.anim.fade_out);
        ft.replace(R.id.frame_fragment_content_control, signInFragment);
        ft.commit();
    }

    private void updateUser(){
        StatePreference state = new StatePreference(AuthenticationActivity.this);
        UpdateUserTask update = new UpdateUserTask(AuthenticationActivity.this,
                state.getUserName(), state.getPassword());
        update.execute();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        delayedHide(10);
    }

    ////////////////////////////////////////////////////////////////////////////////////
    ///Animation zone///////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////

    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {

            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };

    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mControlsView.setVisibility(View.GONE);

        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }
}
