package com.cmu.jaylerr.bikesquad.authentication.background;

import android.app.Activity;

import com.cmu.jaylerr.bikesquad.R;
import com.cmu.jaylerr.bikesquad.models.modelsrequest.RegisterRequest;
import com.cmu.jaylerr.bikesquad.utility.dialogmodels.DialogWarningMessageUtility;
import com.cmu.jaylerr.bikesquad.utility.sharedstring.SharedFlag;

/**
 * Created by jaylerr on 10-May-17.
 */

public class MemberValidator {
    Activity activity;
    RegisterRequest request;

    public MemberValidator(Activity activity, RegisterRequest request) {
        this.activity = activity;
        this.request = request;
    }

    public Boolean isRequestForm(){
        if (isEmailForm()){
            if (isName()){
                if (isPassword()){
                    return true;
                }else {
                    return false;
                }
            }else {
                warningDialog(activity.getString(R.string.authen_warning_name_require));
                return false;
            }
        }else {
            warningDialog(activity.getString(R.string.authen_warning_username_require));
            return false;
        }
    }

    private Boolean isPassword(){
        if (request.getPassword().length() >= 8){
            if (request.getPassword().equals(request.getRepassword())){
                return true;
            }else {
                warningDialog(activity.getString(R.string.authen_warning_password));
                return false;
            }
        }else {
            warningDialog(activity.getString(R.string.authen_warning_password_require));
            return false;
        }
    }

    private Boolean isName(){
        if (request.getFirstname().isEmpty() || request.getLastname().isEmpty()){
            return false;
        }else return true;
    }

    private Boolean isEmailForm(){
        if (request.getEmail().contains("@")){
            String []mail = request.getEmail().split("@");
            if (mail[1].contains(".")){
                return true;
            }else return false;
        }else return false;
    }

    private void warningDialog(final String string){
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                DialogWarningMessageUtility dialog = new DialogWarningMessageUtility(activity,
                        activity.getString(R.string.dialog_title_warning), string, SharedFlag._flag_do_nothing);
                dialog.show();
            }
        });
    }
}
