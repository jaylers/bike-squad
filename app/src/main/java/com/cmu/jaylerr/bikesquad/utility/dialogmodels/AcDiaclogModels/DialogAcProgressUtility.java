package com.cmu.jaylerr.bikesquad.utility.dialogmodels.AcDiaclogModels;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.view.View;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

/**
 * Created by jaylerr on 24-Feb-17.
 */

public class DialogAcProgressUtility extends ProgressDialog{
    Context ctx;
    Activity activity;
    View view;
    ACProgressFlower dialog;

    public DialogAcProgressUtility(Context ctx, View view) {
        super(ctx);
        this.ctx = ctx;
        this.activity = (Activity) ctx;
        this.view = view;
    }

    public DialogAcProgressUtility(Context ctx) {
        super(ctx);
        this.ctx = ctx;
        this.activity = (Activity) ctx;
    }

    public void displayFlower(){
        dialog = new ACProgressFlower.Builder(activity)
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.RED)
                .bgAlpha(0)
                .fadeColor(Color.DKGRAY)
                .build();
        dialog.show();
    }

    public void displayFlower(final String title){
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ACProgressFlower dialog = new ACProgressFlower.Builder(activity)
                        .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                        .themeColor(Color.RED)
                        .bgAlpha(0)
                        .text(title)
                        .fadeColor(Color.WHITE)
                        .build();
                dialog.show();
            }
        });
    }
}
